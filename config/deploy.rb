lock '3.1.0'
set :domain,          ENV["domain"]
set :user,            ENV["user"]
set :user, :ubuntu
set :domain, "78.47.111.27"

set :destination,     ENV["destination"] || fetch(:domain)
set :web_conf,        ENV["web_conf"]    || ENV["environment"] || 'production'

raise "please set domain=app.domain.name.com" unless fetch(:domain)
raise "please set user=server_username"       unless fetch(:user)


set :deploy_via,      :copy
set :copy_strategy,   :export

set :application,     'corpo_system'
set :copy_remote_dir, "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :deploy_to,   fetch(:copy_remote_dir)

role :web, fetch(:destination)
role :app, fetch(:destination)
role :db,  fetch(:destination), :primary => true

set :linked_dirs, %w(log pids sockets)
set :repo_url, 'git@github.com:SergeyKutsko/corpa_system.git'


def get_binding
  binding
end

def from_template(file)
  require 'erb'
  template = File.read(File.join(File.dirname(__FILE__), "..", file))
  result = ERB.new(template).result(get_binding)
end

namespace :nginx do
  %w[start stop restart reload force-reload status configtest].each do |t|
    task t.to_sym do
      on roles(:all) do
        execute "sudo service nginx #{t}"
      end
    end
  end
end

namespace :puma do
  task :fix_socket_perms do
    on roles(:all) do
      execute "sudo chmod 0777 #{fetch(:deploy_to)}/shared/sockets/puma.sock"
    end
  end

  task :stop do
    on roles(:all) do
      execute "[ -f #{fetch(:deploy_to)}/shared/pids/puma.pid ] || exit 0 && kill -9 `cat #{fetch(:deploy_to)}/shared/pids/puma.pid`"
      execute "rm -rf #{fetch(:deploy_to)}/shared/pids/puma.pid"
      execute "rm -rf #{fetch(:deploy_to)}/shared/sockets/puma.sock"
    end
  end

  task :start do
    on roles(:all) do
      within current_path do
        execute :bundle, 'exec', :puma, "-C #{fetch(:puma_conf)}"
      end
    end
  end

  task restart: [:stop, :start] do
    on roles(:all) do

    end
  end

end

after "puma:start", "puma:fix_socket_perms"




task :preconfigure do
  on roles(:all) do
    [ { erb: "puma.rb.erb", name: "puma.rb"},
      { erb: "nginx.conf.erb", name: "nginx.conf", link_to: '/etc/nginx/'},
      { erb: "#{fetch(:application)}.conf.erb", name: "#{fetch(:application)}.conf", link_to: '/etc/nginx/sites-enabled/'},
      { erb: "puma_upstart.conf.erb", name: "pumaupstart.conf", copy_to: '/etc/init/'}
    ].each do |f|
      io = StringIO.new from_template("config/#{f[:erb]}")
      upload! io,  "#{shared_path}/#{f[:name]}"
      execute :sudo, "ln -sf #{shared_path}/#{f[:name]} #{f[:link_to]}#{f[:name]}" if f[:link_to]
      execute :sudo, "cp #{shared_path}/#{f[:name]} #{f[:copy_to]}#{f[:name]}" if f[:copy_to]
    end

  end
end

after "deploy:updated", :preconfigure
before "deploy:started", "deploy:crete_user_and_databse"


namespace :deploy do

  task :crete_user_and_databse do
    on roles(:all) do
      require 'yaml'
      config = YAML::load(File.read(File.join(File.dirname(__FILE__), 'database.yml')))
      execute "echo \"CREATE USER postgres WITH PASSWORD '#{config["production"]["password"]}';\" | sudo -su postgres psql"
      execute "echo 'CREATE DATABASE #{config["production"]["database"]};' | sudo -su postgres psql"
      execute "echo 'GRANT ALL PRIVILEGES ON DATABASE postgres to postgres;' | sudo -su postgres psql"
      execute "sudo -u postgres psql -U postgres -d postgres -c \"alter user postgres with password '#{config["production"]["password"]}';\""
    end
  end

  desc 'Restart application'
  task :restart do

  end

  after :publishing, :restart
  after :restart, "puma:restart"
  after "puma:restart", "nginx:restart"


  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
