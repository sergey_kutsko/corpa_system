# Load the Rails application.
require File.expand_path('../application', __FILE__)
Kernel.load 'config/settings.rb'

# Initialize the Rails application.
CorpaSystem::Application.initialize!
