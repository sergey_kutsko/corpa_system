class UserMailer < ActionMailer::Base
  default from: "from@example.com"
  def welcome_email(user)
    @user = user
    @url  = 'http://korpo-system.com.ua/admin'
    mail(to: @user.email, subject: 'Добро пожаловать в виртуальный офис')
  end
end
