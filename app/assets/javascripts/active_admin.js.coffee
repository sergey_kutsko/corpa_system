#= require active_admin/base
#= require jquery.joyride-2.1.js

$ ->
  if $('#filters_sidebar_section .filter_form_field').length  == 0
	  $('#filters_sidebar_section').hide();
  $('#help_guide').click(->
    $('#joyRideTipContent').joyride(
      autoStart : true
    )
    modal:true
    expose: true
  )
