 class Ability
    include CanCan::Ability

    def initialize(user)
      if user.roles.include? "manager"
        can [:update, :read], Blog, admin_user_id: user.id
        can :create, Blog
        can [:update, :read], AdminUser, id: user.id
        can :manage, PriceList, priceable_id: user.blogs.map(&:id)
        can :create, PriceList
      end
      can :manage, :all if user.roles.include? "admin"
      cannot :read, ActiveAdmin::Page, :name => "Dashboard"
    end

  end