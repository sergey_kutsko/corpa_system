class Factory < ActiveRecord::Base
  has_many :price_lists, as: :priceable, dependent: :destroy
  attr_accessible :name
end
