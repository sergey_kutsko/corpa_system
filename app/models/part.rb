class Part < ActiveRecord::Base

  after_initialize :set_defaults
  TYPES = [%w[window] * 2, %w[door] * 2]
  has_and_belongs_to_many :constructions
  has_one :photo, as: :imageable
  attr_accessible :with_slider, :min_width, :max_width, :min_height, :max_height, :part_type, :possition, :group

  def type_image
  	"#{data_kind}.png"
  end

  def data_kind
  	part_type.downcase['window'] ? 'leaf-x' : 'door-l'
  end

  def set_defaults
    self.min_width  ||= 500
    self.max_width  ||= 950
    self.min_height ||= 500
    self.max_height ||= 1500
    self.part_type  ||= TYPES[0][0]
    self.possition  ||= 1
    self.group      ||= 1
  end

end
