class Construction < ActiveRecord::Base
  DOORS = %W[door-l door-lu]
  LEAFS = %W[leaf-x leaf-l leaf-lu leaf-r leaf-ru]

  default_scope { order('possition') }

  has_and_belongs_to_many :parts, order: 'possition ASC'
  attr_accessible :name, :photo_attributes, :parts_attributes, :possition
  has_one :photo, as: :imageable, dependent: :destroy

  accepts_nested_attributes_for :photo
  accepts_nested_attributes_for :parts, allow_destroy: true

  validates :name, :possition, presence: true

  def groups
    parts.group_by &:group
  end

end
