class User < ActiveRecord::Base

  before_create :set_name
  has_many :comments, dependent: :destroy

  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :registerable

  attr_accessible :provider, :uid, :name, :avatar_url,
                  :email, :password, :password_confirmation, :remember_me

  devise :omniauthable, :omniauth_providers => [:facebook, :vkontakte]


  def self.find_for_oauth(auth, signed_in_resource=nil)
    unless user = User.where(:provider => auth.provider, :uid => auth.uid.to_s).first
      User.create(
        name: auth.info.name,
        provider: auth.provider,
        uid: auth.uid.to_s,
        email: auth.info.email || "#{auth.uid}@#{auth.provider}.vk",
        password: Devise.friendly_token[0,20],
        avatar_url: auth.info.image
      )
    else
      user.update_attributes(avatar_url: auth.info.image, name: auth.info.name)
      user
    end
  end


  def avatar_or_no_user_url
    self.avatar_url || "no_user.jpg"
  end


  private


  def set_name
    self.name ||= email
  end

end

