class City < ActiveRecord::Base
  validates :name, presence: true
  has_many :blogs
  attr_accessible :name
end
