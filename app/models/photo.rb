class Photo < ActiveRecord::Base
  TMP_UPLOAD_DIR = 'tmp/uploads'
  belongs_to :imageable, polymorphic: true
  attr_accessible :file_name, :mime_type, :image_base64, :tmp_file, :image_bin, :description, :file


  def file=(temp_file)
    self.attributes = if ActionDispatch::Http::UploadedFile == temp_file.class
      Photo.attributes_from_temp_file(temp_file)
    else
      Photo.attributes_from_fs_file(temp_file)
    end
  end


  def self.update_base64_string(photo_id)
    photo = Photo.find(photo_id)
    photo.image_base64 = Base64.encode64(File.read(self.get_full_path_to_tmp_file(photo)))
    raise "Photo id=#{photo.id} was not updated" unless photo.save
    File.delete(self.get_full_path_to_tmp_file(photo))

  end

  def self.get_full_path_to_tmp_file(file_name)
    Rails.root.join(TMP_UPLOAD_DIR, file_name)
  end

  def self.instance_from_fs_file(file_path)
    self.new(self.attributes_from_fs_file(file_path))
  end

  def self.attributes_from_fs_file(file_path, description = "" ,type = 'image/png')
    path = file_path.to_s
    file_name = path.split('/').last
    file = File.read(path)
    {
      image_base64: Base64.encode64(file),
      image_bin: file,
      file_name: file_name,
      mime_type: type,
      description: description
    }
  end


  def self.attributes_from_temp_file(file_stream, description="")
    file = file_stream.read
    {
      image_base64: Base64.encode64(file),
      image_bin: file,
      file_name: file_stream.original_filename,
      mime_type: file_stream.content_type,
      description: description
    }
  end

  def self.copy_uploaded_file( temp_file )
    new_tmp_file = temp_file.original_filename
    path = Pathname.new(FileUtils.mkdir_p(Rails.root.join(TMP_UPLOAD_DIR))[0]).join(new_tmp_file)
    File.open(path, "wb") do |f|
      f.write temp_file.read
    end
    path
  end

  def photo
     "data:#{self.mime_type};base64,#{self.image_base64}"
  end

end
