class XlsPriceParser
  COLUMN_RANGE =  ["C".."R", 3]
  ROW_RANGE = ["B", 4..19]


  def initialize(file)
    @spreadheet = Roo::Excel.new(file)
    @spreadheet.default_sheet = @spreadheet.sheets.first
    @price_matrix = []
  end


  def x_y_price_matrix
    ROW_RANGE.last.to_a.map do |row|
      COLUMN_RANGE.first.to_a.inject([])  do |memo, column|
        memo << [@spreadheet.cell(ROW_RANGE.first, row), @spreadheet.cell(COLUMN_RANGE.last, column), @spreadheet.cell(row, column)]
        memo
      end
    end.flatten(1).sort
  end

  def area_max_price_values
    x_y_price_matrix.inject({}) do |m, a|
      m.merge!(Hash[a[0] * a[1], a[2]]) do |key, o, n|
        [n, o].max
      end
    end.to_a.map{ |a| [a[0], a[1]] }.sort
  end

end