class PriceList < ActiveRecord::Base
  WINDOWS = %W[4-16-4 4-16-4i 4-24-4 4-24-4i 4-10-4-10-4 4-10-4-10-4i]
  PROFILE = %W[lider lgc]
  WINDOW_TYPES = [%W[Поворотно-отк tilt-swivel], %W[поворотное swivel],  %W[глухое deaf]]
  FINDINGS = %W[roto maco]

  before_save :update_if_exists
  before_save :create_all_price_lists, unless: Proc.new { |price| price.no_price_lists? }

  def find_same_record
    self.class.where(attributes.slice("window_type", "profile", "window", "findings", "priceable_id", "priceable_type")).first
  end

  def update_if_exists
    if model = find_same_record
      @new_record = false
      self.id = model.id
    end
    true
  end

  belongs_to :priceable, polymorphic: true
  attr_reader :files


  attr_accessible :name, :window_type, :profile, :window, :findings, :file, :files, :priceable_id, :priceable_type, :area_sizes_prices_json, :x_y_price_json

  validates :window_type, :profile, :window, presence: true
  validate do |user|
    @file ||= "не может быть пустым"
    errors.add(:files, @file) if @file.class == String
  end

  def create_all_price_lists
    @files.each do |f|
      priceable_type.constantize.find(priceable_id).price_lists.create!(file: f)
    end
  end

  def no_price_lists?
    @files.blank?
  end

  def files=(temp_files)
    @files = temp_files
    self.file = @files.shift
  end

  def name
    name = "#{window_type}_#{profile}_#{window}"
    name = "#{name}_#{findings}" unless 'deaf' == window_type
    name
  end

  def attributes_from_file_name(file_name)
    t, p, w, f = file_name.split('/').last.split('.').first.split('_').map(&:downcase)
    self.window_type = t if window_type.blank?
    self.profile = p        if profile.blank?
    self.window = w         if window.blank?
    self.findings = f       if findings.blank?
  end

  def copy_temp_file_and_return_path(temp_file)
    file_path = File.join("public", temp_file.original_filename)
    FileUtils.cp temp_file.tempfile.path, file_path
    file_path
  end

  def file=(temp_file)
  	file_path = if ActionDispatch::Http::UploadedFile == temp_file.class
  	   copy_temp_file_and_return_path(temp_file)
    else
      temp_file
    end
    @file = parse_price_list(file_path)
  end

  def parse_price_list(file_path)
    begin
      xls = XlsPriceParser.new(file_path)
      self.area_sizes_prices_json = xls.area_max_price_values.to_json
      self.x_y_price_json = xls.x_y_price_matrix.to_json
      attributes_from_file_name(file_path)
      true
    rescue => e
      "это не прайс лист"
    end
  end

end
