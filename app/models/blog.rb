class Blog < ActiveRecord::Base
  acts_as_commentable
  has_one :photo, as: :imageable, dependent: :destroy
  has_many :price_lists, as: :priceable, dependent: :destroy


  scope :filter, -> (id){ where(city_id: id) }

  belongs_to :admin_user
  belongs_to :city
  belongs_to :factory

  attr_accessible :name, :description, :photo_attributes, :admin_user_id, :lat, :long, :factory_id, :city_id, :created_at

  validates :name, :description, :lat, :long, :admin_user_id, :factory_id, :city_id, :photo, presence: true
  validates :lat , numericality: {  greater_than_or_equal_to:  -90, less_than_or_equal_to:  90 }
  validates :long, numericality: {  greater_than_or_equal_to: -180, less_than_or_equal_to: 180 }

  accepts_nested_attributes_for :photo


  def price_lists_json
    factory_prices = factory.price_lists.all.inject({}){ |memo, model| memo[model.name] = JSON.parse(model.area_sizes_prices_json); memo; }
    company_prices = price_lists.all.inject({}){ |memo, model| memo[model.name] = JSON.parse(model.area_sizes_prices_json); memo; }
    factory_prices.merge(company_prices).to_json
  end




end
