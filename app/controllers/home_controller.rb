class HomeController < ApplicationController

  def index
  end

  def user_agreement
  end

  def facebook
    sign_in_and_redirect_or_redirect
  end

  def vkontakte
    sign_in_and_redirect_or_redirect
  end

  def failure
      flash[:error] = env['omniauth.error.type'].to_s
      redirect_to root_path
  end

  private

  def sign_in_and_redirect_or_redirect
    @user = User.find_for_oauth(oauth, current_user)
    if @user.persisted?
      sign_in_and_redirect @user, :event => :authentication
    else
      session["devise.facebook_data"] = oauth
      redirect_to new_user_session_url
    end
  end

  def oauth
    request.env["omniauth.auth"]
  end


end
