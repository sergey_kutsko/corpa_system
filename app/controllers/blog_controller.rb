class BlogController < ApplicationController
  before_action :authenticate_user!, only: [:comments]
  before_action :find_blog, only: [:show, :comments, :constructions, :construction]

  def index
    @blogs = if params[:q]
      Blog.filter(params[:q]).includes(:photo).page(params[:page]).per(9)
    else
      Blog.all.includes(:photo).page(params[:page]).per(9)
    end
  end

  def show

  end

  def comments
  	if request.post?
      @blog.comments.create(comment_params)
    end
    redirect_to blog_path(@blog)
  end

  def constructions
    @constructions = Construction.all.to_a
  end

  def construction
    @construction = Construction.find params[:construction_id]
    render layout: false
  end


  private

   def comment_params
      params[:comment][:user_id] = current_user.id
      params.require(:comment).permit(:comment, :user_id)
    end

  def find_blog
    @blog = Blog.find params[:id]
    @comments = @blog.comments.includes(:user).page(params[:page]).per(10)
  end

end
