module ApplicationHelper

  def image_to_base64(file_name)
  	Photo.instance_from_fs_file("app/assets/images/#{file_name}").photo
  end

  def background_img(image_path)
   "#{request.protocol}#{request.host_with_port}#{asset_path image_path}"
  end

end
