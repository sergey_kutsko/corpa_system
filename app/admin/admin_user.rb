ActiveAdmin.register AdminUser do

  controller do
    def show
      super do |format|
         format.html do
           redirect_to action: :index, error: nil
         end
      end
    end
  end

  filter :email, if: proc{  current_admin_user.roles.include?("admin")  }
  sidebar "Помощь",  partial: 'guides/admin/admin_user', :if => proc{ current_admin_user.roles.include?("admin") }
  sidebar "Помощь",  partial: 'guides/manager/admin_user', :if => proc{ current_admin_user.roles.include?("manager") }



  index do
    column :email
    column :name
    column :current_sign_in_at
    column :last_sign_in_at
    column :roles do |user|
      user.roles.first
    end

    if current_admin_user.roles.include?("admin")
      default_actions
    else
      actions :defaults => false do |post|
        link_to "Изменить", edit_admin_admin_user_path(post), id: :edit_link
      end
    end

  end



  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :name
      f.input :password
      f.input :password_confirmation
      f.input :roles, as: :select, multiple: true, collection: AdminUser::ROLES if current_admin_user.roles.include?("admin")
    end
    f.actions
  end

  controller do
    def permitted_params
      permitted_params_hash = { admin_user: [:email, :password, :password_confirmation, :name] }
      permitted_params_hash[:admin_user] << { roles: [] } if current_admin_user.roles.include?("admin")
      params.permit permitted_params_hash
    end
  end



end
