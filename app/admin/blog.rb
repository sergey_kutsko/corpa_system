ActiveAdmin.register Blog do


  controller do
    def scoped_collection
      resource_class.includes(:factory, :admin_user) # prevents N+1 queries to your database
    end
  end

  sidebar "Добавить прайсы", only: [:show, :edit] do
    ul do
      li link_to("Прайсы", admin_blog_price_lists_path(blog), id: "price_lists_link")
    end
  end

  filter :name, if: proc{  current_admin_user.roles.include?("admin")  }
  filter :admin_user, if: proc{  current_admin_user.roles.include?("admin")  }
  filter :factory, if: proc{  current_admin_user.roles.include?("admin")  }

  sidebar "Помощь",  partial: 'guides/admin/blog', :if => proc{ current_admin_user.roles.include?("admin") }
  sidebar "Помощь",  partial: 'guides/manager/blog', :if => proc{ current_admin_user.roles.include?("manager") }

  form :html => { :multipart => true } do |f|
    f.inputs "Данные офиса" do
      f.input :name
      f.input :description
      f.input :lat
      f.input :long
      f.input :city, as: :select, required: true, collection: City.all.map{ |u| [u.name, u.id] }, include_blank: false
      f.input :factory, as: :select, required: true, collection: Factory.all.map{ |u| [u.name, u.id] }, include_blank: false
      f.input :admin_user, as: :select, required: true, collection: AdminUser.all.map{ |u| [u.name, u.id] }, include_blank: false if current_admin_user.roles.include?("admin")
      f.input :created_at,  :as => :date_picker
    end

    f.semantic_errors "photo"
    f.inputs "Загрузить фото", :for => [:photo, f.object.photo || f.object.build_photo ] do |fm|
      fm.input :file, required: true, :for => :photo, :as => :file, :hint => fm.template.image_tag(fm.object.photo, size: "50x50")
    end

    #f.inputs "Загрузка прайс листов .xls" do
    #  f.input :photo_attributes, as: :file, hint: f.template.image_tag(f.object.photo.try(:photo), size: "50x50")
    #end

    f.actions
  end

  index  do
    column :name
    column :admin_user do |b|
      b.admin_user.name
    end
    column :factory do |b|
      b.factory.name
    end
    column :city do |b|
      b.city.name
    end
    column :description
    column :lat
    column :long
    column :photo do |ad|
      image_tag(ad.photo.photo, size: "50x50") if ad.photo
    end
    if current_admin_user.roles.include?("admin")
      default_actions
    else
      actions :defaults => false do |post|
        link_to "Изменить", edit_admin_blog_path(post), id: :edit_link
      end
    end
  end

  controller do
    before_filter Proc.new{ params[:blog][:admin_user_id] = current_admin_user.id   }, only: :create, if: Proc.new{ current_admin_user.roles.include?("manager") }
  end

end
