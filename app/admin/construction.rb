ActiveAdmin.register Construction do
  menu :label => I18n.t('admin.menu.constructions')

  filter :name

  form :html => { :multipart => true } do |f|
    f.inputs "Construction fields" do
      f.input :name
      f.input :possition
    end

    f.inputs "Иконка", :for => [:photo, f.object.photo || f.object.build_photo ] do |fm|
      fm.input :file, :for => :photo, :as => :file, :hint => fm.template.image_tag(fm.object.photo, size: "50x50")
    end




    f.inputs "Parts" do
    f.has_many :parts, :header => '' do |cf|
      cf.input :min_width
      cf.input :max_width
      cf.input :min_height
      cf.input :max_height
      cf.input :part_type, as: :select, collection: Part::TYPES
      cf.input :possition
      cf.input :group
      cf.input :_destroy, :as=>:boolean, :required => false, :label=>'Удалить'
    end
  end
    f.actions
  end

  show do |ad|
    attributes_table do
      row :name
      row :possition
      row :photo do
        image_tag(ad.photo.photo)
      end
    end
    active_admin_comments
  end

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  # =>
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

end
