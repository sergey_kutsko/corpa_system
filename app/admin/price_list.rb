ActiveAdmin.register PriceList do
  actions :index, :new, :create, :update, :edit, :destroy
  menu false


  sidebar "Помощь",  partial: 'guides/price_list'

  controller do
    belongs_to :factory, :blog, polymorphic: true
  end

  form :html => { :multipart => true } do |f|
    f.inputs do
      f.input :window_type, as: :select, collection: PriceList::WINDOW_TYPES
      f.input :profile , as: :select, collection: PriceList::PROFILE.map{ |el|   [el.capitalize, el] }
      f.input :window  , as: :select, collection: PriceList::WINDOWS.map{ |el|    [el.capitalize, el]  }
      f.input :findings, as: :select, collection: PriceList::FINDINGS.map{ |el| [el.capitalize, el] }
    end

    f.inputs "Загрузка прайс листов .xls" do
      f.input :files, as: :file, input_html: { multiple: "multiple" }
    end

    f.actions
  end

  index do |price|
    column :id
    column :priceable_type
    column :window_type
    column :profile
    column :window
    column :findings
    column :created_at
    column :updated_at
    default_actions
  end



end
