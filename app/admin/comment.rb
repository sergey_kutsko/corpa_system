ActiveAdmin.register Comment,  as: "PostComment" do
  menu :label => I18n.t('admin.menu.office_comments')

  form do |f|
    f.inputs "Comment" do
      f.input :user
      f.input :comment
      f.input :title
    end
    f.actions
  end

  show do
    column :id
    column :user_id
    column :comment
    column :title
    active_admin_comments
  end




  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

end
