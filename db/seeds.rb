[Construction, PriceList, Factory].each do |r|
  r.all.map(&:destroy)
end

1.upto(4) do |i|
  construction = Construction.new(name: "Окно #{i}-x", possition: i, photo_attributes: { file: Rails.root.join("app/assets/images/alt/#{i}.png") })
  (1..i).to_a.map{ |i| Part.new(possition: i) }.each do |p|
    construction.parts << p
  end
  construction.save
end

door1 = Construction.new(name: "Дверь", possition: 5, photo_attributes: { file: Rails.root.join("app/assets/images/alt/5.png") })
door1.parts << Part.new(min_height: 1900, max_height: 2400, part_type: "door")
door1.save

door2 = Construction.new(name: "Балк. блок", possition: 6, photo_attributes: { file: Rails.root.join("app/assets/images/alt/7.png") })

door2.parts <<  Part.new(possition: 1)
door2.parts <<  Part.new(possition: 2)

door2.parts << Part.new(possition: 3, group: 2, min_height: 1900, max_height: 2400, part_type: "door")

door2.save

factories = ["Статус", "Альпари", "Престиж"].map{ |name| Factory.create name: name }

Dir["pricelists/*.xls"].each do |f|
  puts f

  type, profile, window, findings = f.split('/').last.split('.').first.split('_').map(&:downcase)
  file = Rails.root.join(f).to_s
  factories.each do |factory|
    factory.price_lists.create!(window_type: type, profile: profile, window: window, findings: findings, file: file )
  end
end