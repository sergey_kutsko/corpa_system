class CreatePriceLists < ActiveRecord::Migration
  def change
    create_table :price_lists do |t|
      t.references :priceable, :polymorphic => true
      t.string :name
      t.string :window_type
      t.string :profile
      t.string :window
      t.string :findings
      t.text :area_sizes_prices_json
      t.text :x_y_price_json
      t.timestamps
    end
  end
end
