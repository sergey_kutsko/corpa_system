class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :file_name,  null: false
      t.text :image_base64,  null: false
      t.binary :image_bin,  null: false
      t.string :mime_type,  null: false
      t.string :description,  null: false
      t.references :imageable, polymorphic: true
      t.timestamps
    end
  end
end
