class CreateParts < ActiveRecord::Migration
  def change
    create_table :parts do |t|
      t.boolean :with_slider
      t.integer :min_width
      t.integer :max_width
      t.integer :min_height
      t.integer :max_height
      t.string  :part_type
      t.integer :possition
      t.integer :group
      t.timestamps
    end
  end
end
