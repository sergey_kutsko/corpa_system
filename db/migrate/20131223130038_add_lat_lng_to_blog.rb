class AddLatLngToBlog < ActiveRecord::Migration
  def change
  	add_column :blogs, :lat, :float
  	add_column :blogs, :long, :float
  end
end
