class CreateConstructionsParts < ActiveRecord::Migration
  def change
    create_table :constructions_parts, id: false do |t|
      t.references :construction, :part
    end

    add_index :constructions_parts, [:construction_id, :part_id]
  end


end
