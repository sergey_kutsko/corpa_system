class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.references :admin_user
      t.references :factory
      t.string :name,  null: false
      t.string :description,  null: false
      t.timestamps
    end
  end
end
